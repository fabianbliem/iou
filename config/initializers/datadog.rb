# config/initializers/datadog.rb

options = {
  service_name: 'iou',
  env: Rails.env
}

Datadog.configure do |c|
  c.tracer hostname: 'localhost'  #replace with your hostname
  c.use :rails, options
  c.analytics_enabled = true
  c.tracer enable: true
end




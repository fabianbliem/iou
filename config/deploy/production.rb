set :port, 5412
set :user, 'deployer'
set :deploy_via, :remote_cache
set :use_sudo, true

# Extended Server Syntax
# ======================
# This can be used to drop a more detailed server definition into the
# server list. The second argument is a, or duck-types, Hash and is
# used to set extended properties on the server.

server '193.170.119.191', port: fetch(:port), user: fetch(:user), roles: %w{app db web}
